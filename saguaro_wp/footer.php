<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
	<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
        <?php get_template_part( 'footer-widget' ); ?>
		<div class="container pt-3 pb-3">
            <div class="site-info">




<section class="footer-field">
    <div class="copyright-block">
<p>
                <script>
                        document.write("© " +new Date().getFullYear()+"   Unity Church of San Antonio." );
                </script>
                            <i> <?php
                                        /* translators: 1: Theme name. */
                                        printf( esc_html__( ' All Rights Reserved.' ), 'wp-bootstrap-starter' );
                                ?></i>
</p>
</div><!--copyright-->
<div class="social-media-links">
<?php
if ( is_active_sidebar( 'custom-footer-widget' ) ) : ?>
    <div id="footer-widget-area" class="footer-widget-area widget-area" role="complementary">
    <?php dynamic_sidebar( 'custom-footer-widget' ); ?>
    </div>

<?php endif; ?>
</div>

<div class="siteby-block">
<?php
printf(esc_html__("Website by :"), "Website by :");
?>

                                <a href="<?php echo esc_url( 'https://www.oneeach.com/' ); ?>"><?php esc_html_e( '  OneEach Technologies', 'wp-bootstrap-starter' ); ?></a>
</div><!--siteby-->
</section>




          

                        </div><!--footer-bottom-content-->
            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
