<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-lg-8">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Requested page not found: ( Error page )', 'wp-bootstrap-starter' ); ?></h1><hr>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'Missing Page? We are sorry, but we cannot locate the page you are looking for. You may have followed an outdated link or perhaps you typed in an invalid URL (web address). One of the following steps may help you find what you are looking for', 'wp-bootstrap-starter' ); ?></p>
<p><?php esc_html_e('1. Click the back button on your browser to return to the previous page.', 'wp-bootstrap-starter' );?></p>
<p><?php esc_html_e('2. Correct the URL you have requested.', 'wp-bootstrap-starter' );?></p>
<p><?php esc_html_e('3. Return to the homepage.', 'wp-bootstrap-starter' );?></p>
<p><?php esc_html_e('4. Use the search bar at the top-right of this page.', 'wp-bootstrap-starter' );?></p>
					<?php
					//	get_search_form();


					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
