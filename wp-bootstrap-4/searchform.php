<form role="search" method="get" class="searchform wp-bootstrap-4-searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="text" id="search" class="s form-control" name="s" placeholder="<?php esc_attr_e( 'Search&hellip;', 'wp-bootstrap-4' ); ?>" value="<?php the_search_query(); ?>" >
</form>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<i id="searchIcon" class="fa fa-search fa-lg" aria-hidden="true"></i>
<i id="clear" class="fa fa-times" aria-hidden="true"></i>

