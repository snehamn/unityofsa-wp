</div><!-- /.row -->
 
</div><!-- /.container -->
 
<footer id="colophon" class="site-footer text-center bg-white mt-4 text-muted">
<div class="container">
				<div class="row">
					<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<div class="col">
							<aside class="widget-area footer-1-area mb-2">
								<?php dynamic_sidebar( 'footer-1' ); ?>
							</aside>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<div class="col">
							<aside class="widget-area footer-2-area mb-2">
								<?php dynamic_sidebar( 'footer-2' ); ?>
							</aside>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<div class="col">
							<aside class="widget-area footer-3-area mb-2">
								<?php dynamic_sidebar( 'footer-3' ); ?>
							</aside>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
						<div class="col">
							<aside class="widget-area footer-4-area mb-2">
								<?php dynamic_sidebar( 'footer-4' ); ?>
							</aside>
						</div>
					<?php endif; ?>
				</div>
				<!-- /.row -->
			</div>
		</section>
                <div class="container">
			<div class="site-info">
				<a href="<?php echo esc_url( 'http://getbootstrap.com/' ); ?>"><?php esc_html_e( 'Bootstrap 4 WordPress Theme', 'bootstrapstarter' ); ?></a>
				<span class="sep"> | </span>
				<?php
					/* translators: 1: Theme name. */
					printf( esc_html__( 'Theme Name: %1$s.', 'bootstrapstarter' ), 'bootstrapstarter' );
				?>
			</div><!-- .site-info -->
		</div>
		<!-- /.container -->

<!--    <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
    <p><a href="#">Back to top</a></p>-->
</footer>
<?php wp_footer(); ?>
</body>
</html>
