<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
 
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
 
<body <?php body_class(); ?>>
 
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bootstrap starter' ); ?></a>
	<header id="masthead" class="site-header <?php if ( get_theme_mod( 'sticky_header', 0 ) ) : echo 'sticky-top'; endif; ?>">
                <nav id="site-naviigation" class="main-navigation navbar navbar-expand-lg navbar-dark bg-dark">
                        <?php if( get_theme_mod( 'header_within_container', 0 ) ) : ?><div class="container"><?php endif; ?>
				<?php the_custom_logo(); ?>
                                <a class="main-navigation navbar navbar-expand-lg navbar-dark bg-dark-item active" href="#">Home</a>
                                <a class="main-navigation navbar navbar-expand-lg navbar-dark bg-dark-item" href="#">New features</a>
                                <a class="main-navigation navbar navbar-expand-lg navbar-dark bg-dark-item" href="#">Press</a>
                                <a class="main-navigation navbar navbar-expand-lg navbar-dark bg-dark-item" href="#">New hires</a>
                                <a class="main-navigation navbar navbar-expand-lg navbar-dark bg-dark-item" href="#">About</a>
               </nav>
        </header>
</div>
<!-- 
<div class="container">
 
    <div class="blog-header">
        <h1 class="blog-title">The Bootstrap Blog</h1>
        <p class="lead blog-description">The official example template of creating a blog with Bootstrap.</p>
    </div>--->
 
    <div class="row">
